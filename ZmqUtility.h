#ifndef ZMQ_UTILITY_H
#define ZMQ_UTILITY_H

#define ZMQ_UTILITY_USE_OPENCV

#include <stdio.h>
#include <stdlib.h>
#include <iostream>

#include <eigen3/Eigen/Dense>

#ifdef ZMQ_UTILITY_USE_OPENCV
#include <opencv2/opencv.hpp>    // UBUNTU
//#include <opencv2\opencv.hpp> //WINDOWS
#endif

/*	ZMQ COMUNICATION PROTOCOL

Subscriber Adress : "tcp://127.0.0.1:Port"
Publisher Adress : "tcp://*:Port"

----  PORTS  ----

ACK Comunication --> 100x

x  = n. service that publish the message


STD Comunication --> 5yzz

y  = n. service that publish the message
zz = n. message

*/

namespace MiroZmq
{
	typedef struct _ZmqAck
	{
		unsigned char _type;	// 10
		bool _active;
		int _counter;
		int _pkgSize;						// package size depend from the serilization (how many variable we are sending)

		_ZmqAck()
		{
			_type = 10;
			_pkgSize = 0;
		}

		void SetData(bool active, int counter)
		{
			_active = active;
			_counter = counter;
			_pkgSize = sizeof(unsigned char) + sizeof(bool) + sizeof(int);
		}

	} ZmqAck;

	typedef struct _ZmqEncoderTick
	{
		unsigned char _type;	// 11
		int _rightTick;
		int _leftTick;
		int _pkgSize;						// package size depend from the serilization (how many variable we are sending)

		_ZmqEncoderTick()
		{
			_type = 11;
			_pkgSize = 0;
		}

		void SetData(int rightTick, int leftTick)
		{
			_rightTick = rightTick;
			_leftTick = leftTick;
			_pkgSize = sizeof(unsigned char) + sizeof(int) + sizeof(int);
		}

	} ZmqEncoderTick;

	typedef struct _ZmqWheelVelocity
	{
		unsigned char _type;	// 12
		float _rightSpeed;
		float _leftSpeed;
		int _pkgSize;						// package size depend from the serilization (how many variable we are sending)

		_ZmqWheelVelocity()
		{
			_type = 12;
			_pkgSize = 0;
		}

		void SetData(float rightVelocity, float leftVelocity)
		{
			_rightSpeed = rightVelocity;
			_leftSpeed = leftVelocity;
			_pkgSize = sizeof(unsigned char) + sizeof(float) + sizeof(float);
		}

	} ZmqWheelVelocity;

	typedef struct _ZmqBrake
	{
		unsigned char _type;	// 13
		bool _brakeEnable;
		bool _brakeStatus;
		int _pkgSize;						// package size depend from the serilization (how many variable we are sending)

		_ZmqBrake()
		{
			_type = 13;
			_pkgSize = 0;
		}

		void SetData(bool brakeEnable, bool brakeStatus)
		{
			_brakeEnable = brakeEnable;
			_brakeStatus = brakeStatus;
			_pkgSize = sizeof(unsigned char) + sizeof(bool) + sizeof(bool);
		}

	} ZmqBrake;

	enum NavigationStatus { Idle, Manual, SemiAuto, Tilt, Sleep, Error };

	typedef struct _ZmqStatus
	{
		unsigned char _type;	// 14
		NavigationStatus _status;
		int _param;							// Error = 0  --> System not in error
		// Error = 10 ->> Ack Error from Hmi
		// Error = 12 ->> Ack Error from Control Service
		// Error = 12 ->> Ack Error from Comunication Service
		int _pkgSize;						// package size depend from the serilization (how many variable we are sending)

		_ZmqStatus()
		{
			_type = 14;
			_pkgSize = 0;
		}

		void SetData(NavigationStatus status, int param)
		{
			_status = status;
			_param = param;
			_pkgSize = sizeof(unsigned char) + sizeof(int) + sizeof(int);
		}

	} ZmqStatus;

#ifdef ZMQ_UTILITY_USE_OPENCV
	typedef struct _ZmqMat
	{
		unsigned char _type;	// 15
		int _rows;
		int _cols;
		int _dataType;
		int _depth;
		int _channels;
		unsigned char *_data;
		unsigned long _pkgSize;						// package size depend from the serilization (how many variable we are sending)

		_ZmqMat()
		{
			_type = 15;
			_pkgSize = 0;
		}

		void SetData(cv::Mat &data)
		{
			_rows = data.rows;
			_cols = data.cols;
			_dataType = data.type();
			_depth = data.depth();
			_channels = data.channels();
			_data = data.data;
			_pkgSize = sizeof(unsigned char) + sizeof(int) + sizeof(int) + sizeof(int) + (data.total() * data.elemSize());
		}

	} ZmqMat;
#endif

	typedef struct _Zmq3DPoints
	{
		unsigned char _type;	// 16
		int _rows;
		int _cols;
		signed short *_xPoints;				// measured in millimeters 
		signed short *_yPoints;
		signed short *_zPoints;
		unsigned long _pkgSize;

		_Zmq3DPoints()
		{
			_type = 16;
			_pkgSize = 0;
		}

		void SetData(int rows, int cols, signed short *xPoints, signed short *yPoints, signed short *zPoints)
		{
			_rows = rows;
			_cols = cols;
			_xPoints = xPoints;
			_yPoints = yPoints;
			_zPoints = zPoints;
			_pkgSize = sizeof(unsigned char) + sizeof(int) + sizeof(int) + (3 * (sizeof(signed short)* _rows * _cols));
		}


	} Zmq3DPoints;

	typedef struct _Zmq2DPoints
	{
		unsigned char _type;	// 17
		int _rows;
		int _cols;
		signed short *_xPoints;				// measured in millimeters 
		signed short *_yPoints;
		unsigned long _pkgSize;

		_Zmq2DPoints()
		{
			_type = 17;
			_pkgSize = 0;
		}

		void SetData(int rows, int cols, signed short *xPoints, signed short *yPoints)
		{
			_rows = rows;
			_cols = cols;
			_xPoints = xPoints;
			_yPoints = yPoints;
			_pkgSize = sizeof(unsigned char) + sizeof(int) + sizeof(int) + (2 * (sizeof(signed short)* _rows * _cols));
		}


	} Zmq2DPoints;

	typedef struct
	{
		unsigned char _type;	// 18
		int _nPaths;
		int _nPoints;
		signed short	*_ids;
		signed short	*_xPoints;				// measured in meters 
		signed short	*_yPoints;
		signed short	*_zPoints;
		unsigned char	*_busyPoints;
		unsigned long	_pkgSize;

		void SetData(int nPaths, int nPoints, signed short *ids, signed short *xPoints, signed short *yPoints, signed short *zPoints, unsigned char	*busyPoints)
		{
			_type = 18;
			_nPaths = nPaths;
			_nPoints = nPoints;
			_ids = ids;
			_xPoints = xPoints;
			_yPoints = yPoints;
			_zPoints = zPoints;
			_busyPoints = busyPoints;
			_pkgSize = sizeof(unsigned char) + sizeof(int) + sizeof(int) + (sizeof(signed short)*_nPaths*(1 + (3 * _nPoints))) + (sizeof(unsigned char)*_nPoints*_nPaths);
		}

	} ZmqPoisPathOld;

	typedef struct _ZmqPoisPath
	{
		unsigned char _type;	// 18
		int _nPaths;
		signed short	*_ids;
		signed short	*_nPoints;
		signed short	*_xPoints;				// measured in meters 
		signed short	*_yPoints;
		signed short	*_zPoints;
		float			*_transformationData;
		unsigned char	*_busyPoints;
		unsigned long	_pkgSize;

		_ZmqPoisPath()
		{
			_type = 18;
			_nPaths = 0;
			_pkgSize = 0;
		}

		void SetData(int nPaths, signed short *ids, signed short *nPoints, signed short *xPoints, signed short *yPoints, signed short *zPoints, float *trMatrixData, unsigned char *busyPoints)
		{
			std::cout << "incomingNPaths " << _nPaths << std::endl;
			_nPaths = nPaths;
			_nPoints = nPoints;
			_ids = ids;
			_xPoints = xPoints;
			_yPoints = yPoints;
			_zPoints = zPoints;
			_transformationData = trMatrixData;
			_busyPoints = busyPoints;

			unsigned long nPointsPaths = 0;
			for (int ii = 0; ii < _nPaths; ii++)
			{
				nPointsPaths += _nPoints[ii];
			}

			_pkgSize = sizeof(unsigned char) + sizeof(int) + sizeof(signed short)*_nPaths + sizeof(signed short)*_nPaths + (3 * sizeof(signed short)* nPointsPaths) + sizeof(float) * 16 * _nPaths + sizeof(unsigned char)*_nPaths;

		}


	} ZmqPoisPath;

	typedef struct _ZmqEigenFloatMatrix
	{
		unsigned char _type;	// 19
		int _rows;
		int _cols;
		int _elementSize;
		float *_data;
		unsigned long _pkgSize;						// package size depend from the serilization (how many variable we are sending)

		_ZmqEigenFloatMatrix()
		{
			_type = 19;
			_pkgSize = 0;
		}

		void SetData(Eigen::MatrixXf &matrix)
		{
			if (_data == NULL)
			{
				_data = new float[matrix.rows() * matrix.cols()];
			}

			_rows = matrix.rows();
			_cols = matrix.cols();
			_elementSize = 4;

			for (int ii = 0; ii < matrix.rows(); ii++)
			{
				for (int jj = 0; jj < matrix.rows(); jj++)
				{
					_data[ii * 3 + jj] = matrix(ii, jj);
					//std::cout << "!!!!!!!!!!!!!!!!!!  SET DATA:: " << _data[ii, jj] << std::endl;
				}
			}

			//_data = matrix.data();
			_pkgSize = sizeof(unsigned char) + sizeof(int) + sizeof(int) + sizeof(int) + (sizeof(float)*_rows*_cols);
		}

	} ZmqEigenFloatMatrix;

	typedef struct _ZmqEigenMatrix3f
	{
		unsigned char _type;	// 19
		int _rows;
		int _cols;
		int _elementSize;
		float _data[9];
		unsigned long _pkgSize;						// package size depend from the serilization (how many variable we are sending)

		_ZmqEigenMatrix3f()
		{
			_type = 19;
			_pkgSize = 0;
		}

		void SetData(Eigen::Matrix3f &matrix)
		{
			_rows = 3;
			_cols = 3;
			_elementSize = 4;

			for (int ii = 0; ii < matrix.rows(); ii++)
			{
				for (int jj = 0; jj < matrix.rows(); jj++)
				{
					_data[ii * 3 + jj] = matrix(ii, jj);
					std::cout << "!!!!!!!!!!!!!!!!!!  SET DATA:: " << _data[ii, jj] << std::endl;
				}
			}
			_pkgSize = sizeof(unsigned char) + sizeof(int) + sizeof(int) + sizeof(int) + (sizeof(float)*_rows*_cols);
		}

	} ZmqEigenMatrix3f;

	typedef struct _ZmqEigenMatrix4f
	{
		unsigned char _type;	// 19
		int _rows;
		int _cols;
		int _elementSize;
		float _data[14];
		unsigned long _pkgSize;						// package size depend from the serilization (how many variable we are sending)

		_ZmqEigenMatrix4f()
		{
			_type = 19;
			_pkgSize = 0;
		}

		void SetData(Eigen::Matrix4f &matrix)
		{
			_rows = 4;
			_cols = 4;
			_elementSize = 4;

			for (int ii = 0; ii < matrix.rows(); ii++)
			{
				for (int jj = 0; jj < matrix.rows(); jj++)
				{
					_data[ii * 4 + jj] = matrix(ii, jj);
					std::cout << "!!!!!!!!!!!!!!!!!!  SET DATA:: " << _data[ii, jj] << std::endl;
				}
			}
			_pkgSize = sizeof(unsigned char) + sizeof(int) + sizeof(int) + sizeof(int) + (sizeof(float)*_rows*_cols);
		}

	} ZmqEigenMatrix4f;

	typedef struct _ZmqPose
	{
		unsigned char _type;	// 20
		float _x;
		float _y;
		float _theta;
		unsigned long _pkgSize;

		_ZmqPose()
		{
			_type = 20;
			_pkgSize = 0;
		}

		void SetData(float x, float y, float theta)
		{
			_type = 20;
			_x = x;
			_y = y;
			_theta = theta;
			_pkgSize = sizeof(unsigned char) + sizeof(float) + sizeof(float) + sizeof(float);
		}

	} ZmqPose;

	typedef struct _ZmqTilt
	{
		unsigned char _type;	// 21
		int _tiltType;
		int _pkgSize;						// package size depend from the serilization (how many variable we are sending)

		_ZmqTilt()
		{
			_type = 21;
			_pkgSize = 0;
		}

		void SetData(int tiltType)
		{
			_tiltType = tiltType;
			_pkgSize = sizeof(unsigned char) + sizeof(int);
		}

	} ZmqTilt;
}


class ZmqUtility
{


public:
	ZmqUtility();
	~ZmqUtility();


	static MiroZmq::ZmqWheelVelocity DeserializeVelocity(char* buf, int size);
	static MiroZmq::ZmqEncoderTick DeserializeEncoderTIck(char* buf, int size);
	static MiroZmq::ZmqAck DeserializeAck(char* buf, int size);
	static MiroZmq::ZmqBrake DeserializeBrake(char *buf, int size);
	static MiroZmq::ZmqStatus DeserializeStatus(char *buf, int size);
#ifdef ZMQ_UTILITY_USE_OPENCV
	static MiroZmq::ZmqMat DeserializeMat(char *buf, int size);
#endif
	static MiroZmq::Zmq3DPoints Deserialize3DPoints(char *buf, int size);
	static MiroZmq::Zmq2DPoints Deserialize2DPoints(char *buf, int size);
	static MiroZmq::ZmqPoisPathOld DeserializePoisPathsOld(char *buf, int size);
	static MiroZmq::ZmqPoisPath DeserializePoisPaths(char *buf, int size);
	static void DeserializePoisPaths(char *buf, int size, MiroZmq::ZmqPoisPath &paths);
	static MiroZmq::ZmqEigenFloatMatrix DeserializeFloatMatrix(char *buf, int size);
    static void DeserializeFloatMatrix(char *buf, int size, MiroZmq::ZmqEigenFloatMatrix &matrix);
	static MiroZmq::ZmqEigenMatrix3f DeserializeMatrix3f(char *buf, int size);
	static void DeserializeMatrix4f(char *buf, int size, MiroZmq::ZmqEigenMatrix4f &matrix);
	static MiroZmq::ZmqPose DeserializePose(char *buf, int size);
    static void DeserializeTilt(char *buf, int size, MiroZmq::ZmqTilt &tilt);

	static int SerializeVelocity(MiroZmq::ZmqWheelVelocity velocity, char* outBuf);
	static int SerializeEncoderTick(MiroZmq::ZmqEncoderTick ticks, char* outBuf);
	static int SerializeAck(MiroZmq::ZmqAck ack, char* outBuf);
	static int SerializeBrake(MiroZmq::ZmqBrake brake, char *outBuf);
	static int SerializeStatus(MiroZmq::ZmqStatus status, char *outBuf);
#ifdef ZMQ_UTILITY_USE_OPENCV
	static int SerializeMat(MiroZmq::ZmqMat mat, char *outBuf);
#endif
	static int Serialize3DPoints(MiroZmq::Zmq3DPoints points, char *outBuf);
	static int Serialize2DPoints(MiroZmq::Zmq2DPoints points, char *outBuf);
	static int SerializePoisPathsOld(MiroZmq::ZmqPoisPathOld paths, char *outBuf);
	static int SerializePoisPaths(MiroZmq::ZmqPoisPath paths, char *outBuf);
	static int SerializeFloatMatrix(MiroZmq::ZmqEigenFloatMatrix paths, char *outBuf);
	static int SerializePose(MiroZmq::ZmqPose pose, char *outBuf);
	static int SerializeTilt(MiroZmq::ZmqTilt tilt, char *outBuf);

};

#endif
