#ifndef ARUCOANALYZER_H
#define ARUCOANALYZER_H
#include <opencv/cv.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/aruco.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/calib3d/calib3d.hpp>
#include <PTRealsense2.h>

#include "PTRealsense2.h"
#include <opencv2/imgproc.hpp>



class ArucoAnalyzer
{
public:
    struct PC {
        signed short *x;
        signed short *y;
        signed short *z;
        unsigned char *r;
        unsigned char *g;
        unsigned char *b;
        size_t size;
    };



    ArucoAnalyzer();
    void MarkerFinder(cv::Mat COLORFRAME);
    void CalibrationCamAruco(rs2_intrinsics ColorIntrinsics);
    void ExtractDistance(cv::Mat DepthMat, float DepthScale, cv::Mat ColorMat);

    cv::Mat cameraMatrix;
    //float distCoeff[];
    //cv::Mat distCoeff;

    cv::Mat DistanceMat;
    cv::Mat DepthColorMat;
    bool MAT_GEN = false;
    cv::Point2f pt1;
     cv::Point2f pt2;
      cv::Point2f pt3;
       cv::Point2f pt4;
       PTRealsense2 D435;






private:



protected:
};

#endif // ARUCOANALYZER_H
