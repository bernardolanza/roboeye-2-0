#pragma once

#ifdef foreach
    #undef foreach
    #include <librealsense2/rs.hpp> // Include RealSense Cross Platform API
    #include <librealsense2/rsutil.h>
    #define foreach Q_FOREACH
#else
    #include <librealsense2/rs.hpp> // Include RealSense Cross Platform API
    #include <librealsense2/rsutil.h>

#endif

#include <opencv2/opencv.hpp>   // Include OpenCV API
#include <opencv2/highgui.hpp>

#define PTRS2_VISUALIZE_IMAGE 0
#define PTRS2_VERBOSE 0

/* Struct that holds a pointer to an rs2_option, we'll store a pointer to a filter this way;
int_params and float_params: maps from an option supported by the filter, to the variable that controls
this option;
string: map from an option supported by the filter, to the corresponding vector of strings that will
be used by the gui;
do_filter: a boolean controlled by the user that determines whether to apply the filter.*/
struct filter_options {
	rs2::options* filter;
	std::map<rs2_option, int> int_params;
	std::map<rs2_option, float> float_params;
	std::map<rs2_option, std::vector<char*> > string;
	bool do_filter;
};

class PTRealsense2
{

private:
    struct PC {
        signed short *x;
        signed short *y;
        signed short *z;
        unsigned char *r;
        unsigned char *g;
        unsigned char *b;
        size_t size;
    };

public:
	PTRealsense2();
	~PTRealsense2();


	bool initializeSensor(cv::Size2i depthRowCol, int depthFPS, cv::Size2i colorRowCol, int colorFPS, float tempFilterAlpha = 1, bool wantIR = false, std::string sn = "");
	bool waitNewFrame();

	void printD435Resolutions();
     bool extract3dPoints();

    bool getDepth(cv::Mat *image);
    bool getColor(cv::Mat *image);
    bool getIr(cv::Mat *image);
    bool getTime(signed long long *time);


	size_t getPointCloud(signed short **x, signed short **y, signed short **z,
                      unsigned char **r = nullptr, unsigned char **g = nullptr, unsigned char **b = nullptr);
    int getMapper(unsigned short **x, unsigned short **y);

    void releaseSensor();
    bool acquire3Dtables(cv::Mat *tableX, cv::Mat *tableY);
    std::string getUid();
    std::string getLastError();
    float _depth_scale;
    PC _pointCloud;
    // depth intrinsics
    rs2_intrinsics _depthIntrinsics;

	
private:
	


	float get_depth_scale(rs2::device dev);


    bool mapColorPoints();
    bool addColorToCloud();
    bool addColorToMapper();

    bool grabDepth();
    bool grabColor();
    bool grabIr();
    bool grabPointCloud(bool coloured);

    void initializeArray(signed short **array, size_t size, int value);
    void initializeArray(unsigned char **array, size_t size, int value);
    void initializeArray(unsigned short **array, size_t size, int value);
    void initializeArray(int **array, size_t size, int value);

	rs2::context _context;
    rs2::pipeline _pipe;
	rs2::config _cfg;
	rs2::pipeline_profile _profile;
	rs2::device _device;
	


	rs2::frame _depthFrameFiltered;
    cv::Mat _depthMat;
	bool _wantStreamDepth;
    bool _depthReaded;

	// color intrinsics
	rs2_intrinsics _colorIntrinsics;
    cv::Mat _colorMat;
	bool _wantStreamColor;
	bool _colorReaded;

	// ir intrinsics
	rs2_intrinsics _irIntrinsics;
	cv::Mat _irMat;
	bool _wantStreamIr;
	bool _irReaded;

	// color 2 depth extrinsics
	rs2_extrinsics _c2dExtrinsics;
	rs2_extrinsics _d2cExtrinsics;

	std::string _uid;
	std::string _lastErrorString;

	rs2::frameset _frames;
	bool _newFramePresent;


	cv::Mat _mapperX, _mapperY;										// Dimensioni mapper come quelle della depth.

    // pointcloud
    int *_colorPixelR, *_colorPixelC;
    bool _pointCloudCalculated;
    bool _mappingCalculated;
	
	/* Decalre filters: decimation, temporal and spatial
	Decimation - reduces depth frame density; Spatial - edge-preserving spatial smoothing;
	Temporal - reduces temporal noise */
	rs2::decimation_filter dec_filter;
	rs2::spatial_filter spat_filter;
	rs2::temporal_filter temp_filter;

	rs2::disparity_transform disp_filter;

	std::vector<filter_options> filters;

	//void set_defaults(std::vector<filter_options>& filters, const std::vector<rs2_option>& option_names);
	//bool is_all_integers(float min, float max, float def, float step);
	//inline bool is_integer(float f);
};

