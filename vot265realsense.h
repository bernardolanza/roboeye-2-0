#ifndef VOT265REALSENSE_H
#define VOT265REALSENSE_H
#include <iostream>
#include <librealsense2/rs.hpp>
#include <librealsense2/rsutil.h>
#include <librealsense2/hpp/rs_frame.hpp>
#include <librealsense2/hpp/rs_types.hpp>
#include <librealsense2/hpp/rs_frame.hpp>
#include <librealsense2/h/rs_types.h>
#include <librealsense2/rs.hpp>
#include <librealsense2/rsutil.h>
#include <librealsense2/hpp/rs_frame.hpp>
#include <librealsense2/hpp/rs_types.hpp>
#include <librealsense2/hpp/rs_frame.hpp>
#include <librealsense2/h/rs_types.h>
#include <opencv2/core.hpp>
#include <opencv2/opencv.hpp>   // Include OpenCV API
//#include <opencv2/highgui.hpp> (original)
#include <opencv2/highgui/highgui.hpp> //bernardo
//#include <rs_frame.hpp>
//#include <camerasinteraction.h>
#include <math.h>
#define PI 3.14159265
// inizio sviluppo classe

//class CamerasInteraction;

class VOT265Realsense
{

public:
    VOT265Realsense();

    //~VOT265Realsense();
    void calcprintXYZ();
    void AllSenseMode();
    void DrawTrajectories();
private:
    rs2::pipeline pipe;
    rs2::config cfg;
    rs2::frameset frames;
    rs2::pipeline_profile pipeline;
    const int map_dim = 1000;
    cv::Point trj[10000];//impongo un limite di punti
    cv::Point pt;
    int thickness = 2;
    cv::Point pax;
    cv::Point pay;
    cv::Mat aximage;
    float angle;
    const int l = 50;
    const int zoom = 500;








protected:
    rs2_pose pose;
    rs2_vector accel_sample;
    rs2_vector gyro_sample;



};

#endif // VOT265REALSENSE_H
