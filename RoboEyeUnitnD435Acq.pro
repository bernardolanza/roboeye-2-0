QT -= gui

CONFIG += c++11 console
CONFIG -= app_bundle

LIBS += -lzmq

# The following define makes your compiler emit warnings if you use
# any feature of Qt which as been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += main.cpp \
    MIRoZmqPublisher.cpp \
    MIRoZmqSubscriber.cpp \
    PTRealsense2.cpp \
    RoboeyeUnitnD435Acq.cpp \
    ZmqUtility.cpp \
    vot265realsense.cpp \
    arucoanalyzer.cpp

HEADERS += \
    MIRoZmqPublisher.h \
    MIRoZmqSubscriber.h \
    PTRealsense2.h \
    RoboeyeUnitnD435Acq.h \
    ZmqUtility.h \
    vot265realsense.h \
    arucoanalyzer.h

DISTFILES += \
    config.ini


# ------------------------------------------------------------------------OPENCV UNIX
unix{
INCLUDEPATH += \
    /usr/local/include/opencv
    #/usr/include/opencv2
LIBS += `pkg-config  --libs opencv`
}



#-----------------------------------------REALSENSE 2

unix{
    REALSENSE2_LIBS_PATH = "/usr/local/lib"
    REALSENSE2_INCLUDE_PATH ="/usr/local/include"

    INCLUDEPATH += $${REALSENSE2_INCLUDE_PATH}

    LIBS += -L$${REALSENSE2_LIBS_PATH} -lrealsense2
}


