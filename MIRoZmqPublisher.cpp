#include "MIRoZmqPublisher.h"


MIRoZmqPublisher::MIRoZmqPublisher(std::string zmqAddress)
{
	_m_address = zmqAddress;
	_m_context = zmq::context_t(1);
	_m_publisher = new zmq::socket_t(_m_context, ZMQ_PUB);
    _m_publisher->bind(_m_address);

    //zmq::context_t context(1);
    //zmq::socket_t publisher(context, ZMQ_PUB);
    //publisher.bind("tcp://*:5563");
}


MIRoZmqPublisher::~MIRoZmqPublisher()
{
}

bool MIRoZmqPublisher::publishString(std::string string2send)
{
	memcpy(_m_buffer, string2send.c_str(), strlen(string2send.c_str()) + 1);

	zmq::message_t message(_m_buffer, strlen(string2send.c_str()) + 1);
	_m_publisher->send(message);

	return 0;
}

bool MIRoZmqPublisher::PublishVelocity(MiroZmq::ZmqWheelVelocity velocity2send)
{
	char *outBuffer = new char[velocity2send._pkgSize];
	int size = ZmqUtility::SerializeVelocity(velocity2send, outBuffer);

	if (size == velocity2send._pkgSize)
	{
		zmq::message_t message(outBuffer, size);
		_m_publisher->send(message);
	}
	else
	{
		std::cout << "!!!!!!!!!!!!!!!!!! PACKAGE VELOCITY SIZE ERROR !!!!!!!!!!!!!!!!!!!!" << std::endl;
		delete outBuffer;
		return 1;
	}

	delete outBuffer;

	return 0;

}

bool MIRoZmqPublisher::publishAck(MiroZmq::ZmqAck ack2send)
{
	char *outBuffer = new char[ack2send._pkgSize];
	int size = ZmqUtility::SerializeAck(ack2send, outBuffer);

	if (size == ack2send._pkgSize)
	{
		zmq::message_t message(outBuffer, size);
		_m_publisher->send(message);
	}
	else
	{
		std::cout << "!!!!!!!!!!!!!!!!!! PACKAGE ACK SIZE ERROR !!!!!!!!!!!!!!!!!!!!" << std::endl;
		delete outBuffer;
		return 1;
	}

	delete outBuffer;

	return 0;

}

bool MIRoZmqPublisher::publishStatus(MiroZmq::ZmqStatus status2send)
{
	char *outBuffer = new char[status2send._pkgSize];
	int size = ZmqUtility::SerializeStatus(status2send, outBuffer);

	if (size == status2send._pkgSize)
	{
		zmq::message_t message(outBuffer, size);
		_m_publisher->send(message);
	}
	else
	{
		std::cout << "!!!!!!!!!!!!!!!!!! PACKAGE STATUS SIZE ERROR !!!!!!!!!!!!!!!!!!!!" << std::endl;
		delete outBuffer;
		return 1;
	}

	delete outBuffer;

	return 0;

}

#ifdef ZMQ_UTILITY_USE_OPENCV
bool MIRoZmqPublisher::publishRgbFrame(MiroZmq::ZmqMat &mat2send)
{
	char *outBuffer = new char[mat2send._pkgSize];
	int size = ZmqUtility::SerializeMat(mat2send, outBuffer);

	if (size == mat2send._pkgSize)
	{
		zmq::message_t message(outBuffer, size);
		_m_publisher->send(message);
	}
	else
	{
		std::cout << "!!!!!!!!!!!!!!!!!! PACKAGE MAT SIZE ERROR !!!!!!!!!!!!!!!!!!!!" << std::endl;
		delete outBuffer;
		return 1;
	}

	delete outBuffer;

	return 0;
}
#endif

bool MIRoZmqPublisher::publish3DPoints(MiroZmq::Zmq3DPoints points2Send)
{
	char *outBuffer = new char[points2Send._pkgSize];
	int size = ZmqUtility::Serialize3DPoints(points2Send, outBuffer);

	if (size == points2Send._pkgSize)
	{
		zmq::message_t message(outBuffer, size);
		_m_publisher->send(message);
	}
	else
	{
		std::cout << "!!!!!!!!!!!!!!!!!! PACKAGE 3D POINTS SIZE ERROR !!!!!!!!!!!!!!!!!!!!" << std::endl;
		delete outBuffer;
		return 1;
	}

	delete outBuffer;

	return 0;

}

bool MIRoZmqPublisher::publish2DPoints(MiroZmq::Zmq2DPoints points2Send)
{
	char *outBuffer = new char[points2Send._pkgSize];
	int size = ZmqUtility::Serialize2DPoints(points2Send, outBuffer);

	if (size == points2Send._pkgSize)
	{
		zmq::message_t message(outBuffer, size);
		_m_publisher->send(message);
	}
	else
	{
		std::cout << "!!!!!!!!!!!!!!!!!! PACKAGE 2D POINTS SIZE ERROR !!!!!!!!!!!!!!!!!!!!" << std::endl;
		delete outBuffer;
		return 1;
	}

	delete outBuffer;

	return 0;

}

bool MIRoZmqPublisher::publishPoisPaths(MiroZmq::ZmqPoisPath points2Send)
{
	char *outBuffer = new char[points2Send._pkgSize];
	int size = ZmqUtility::SerializePoisPaths(points2Send, outBuffer);

	if (size == points2Send._pkgSize)
	{
		zmq::message_t message(outBuffer, size);
		_m_publisher->send(message);
	}
	else
	{
		std::cout << "!!!!!!!!!!!!!!!!!! PACKAGE POI PATH SIZE ERROR !!!!!!!!!!!!!!!!!!!!" << std::endl;
		delete outBuffer;
		return 1;
	}

	delete outBuffer;

	return 0;

}

bool MIRoZmqPublisher::publishFloatMatrix(MiroZmq::ZmqEigenFloatMatrix matrix2Send)
{
	char *outBuffer = new char[matrix2Send._pkgSize];
	int size = ZmqUtility::SerializeFloatMatrix(matrix2Send, outBuffer);

	if (size == matrix2Send._pkgSize)
	{
		zmq::message_t message(outBuffer, size);
		_m_publisher->send(message);
	}
	else
	{
		std::cout << "!!!!!!!!!!!!!!!!!! PACKAGE EIGEN MATRIX SIZE ERROR !!!!!!!!!!!!!!!!!!!!" << std::endl;
		delete outBuffer;
		return 1;
	}

	delete outBuffer;

	return 0;

}

bool MIRoZmqPublisher::publishPose(MiroZmq::ZmqPose pose2Send)
{
	char *outBuffer = new char[pose2Send._pkgSize];
	int size = ZmqUtility::SerializePose(pose2Send, outBuffer);

	if (size == pose2Send._pkgSize)
	{
		zmq::message_t message(outBuffer, size);
		_m_publisher->send(message);
	}
	else
	{
		std::cout << "!!!!!!!!!!!!!!!!!! PACKAGE POSE SIZE ERROR !!!!!!!!!!!!!!!!!!!!" << std::endl;
		delete outBuffer;
		return 1;
	}

	delete outBuffer;

	return 0;

}

