#include "arucoanalyzer.h"

ArucoAnalyzer::ArucoAnalyzer()
{
/*

    cv::Mat markerImage;
    cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    cv::aruco::drawMarker(dictionary, 23, 200, markerImage, 1);
    cv::imwrite("marker23.png", markerImage);
*/


}

 void ArucoAnalyzer::CalibrationCamAruco(rs2_intrinsics ColorIntrinsics){


     std::cout << "intrinsics width: " << ColorIntrinsics.width << std::endl;
     std::cout << "intrinsics height: " << ColorIntrinsics.height << std::endl;
     std::cout << "intrinsics ppx: " << ColorIntrinsics.ppx << std::endl;
     std::cout << "intrinsics ppy: " << ColorIntrinsics.ppy << std::endl;
     std::cout << "intrinsics dist coeff 1..: " << ColorIntrinsics.coeffs[4] << std::endl;
     std::cout << "intrinsics fx: " << ColorIntrinsics.fx << std::endl;
     std::cout << "intrinsics fy: " << ColorIntrinsics.fy << std::endl;


  /*   typedef struct rs2_intrinsics
     {
         int           width;     //< Width of the image in pixels
         int           height;    //< Height of the image in pixels
         float         ppx;       //< Horizontal coordinate of the principal point of the image, as a pixel offset from the left edge
         float         ppy;       //< Vertical coordinate of the principal point of the image, as a pixel offset from the top edge
         float         fx;        //< Focal length of the image plane, as a multiple of pixel width
         float         fy;        //< Focal length of the image plane, as a multiple of pixel height
         rs2_distortion model;     //< Distortion model of the image
         float         coeffs[5]; //< Distortion coefficients
     } rs2_intrinsics;
 */
     //pose extimation
     //camera matrix : 3x3 focal dist and camera center coord
     //dist coeff : 5 or + disortion produced by camera
     //K is the camera intrinsics matrix (this depends on the camera lenses parameters.
     //Every time you change the camera focus for exapmle the focal distances fx and fy values whitin this matrix change)
     //R and T are the extrensics of the camera. They represent the rotation and translation matrices for the camera respecively.
     //These are basically the matrices that represent the camera position/orientation in the 3D world.
     //P_im = K・R・T ・P_world


     //cv::Mat distCoeffs(1,5,CV_64F,ColorIntrinsics.coeffs);
     //distCoeffs = (cv::Mat1f(5, 1) << ColorIntrinsics.coeffs[0],ColorIntrinsics.coeffs[1],ColorIntrinsics.coeffs[2],ColorIntrinsics.coeffs[3],ColorIntrinsics.coeffs[4]);


     cameraMatrix = (cv::Mat_<double>(3,3) <<  D435._depthIntrinsics.fx, 0, D435._depthIntrinsics.ppx, 0, D435._depthIntrinsics.fy, D435._depthIntrinsics.ppy, 0, 0, 1);
     // D435._depthIntrinsics.coeffs[1];
    // distCoeffs = (cv::Mat_<double>(1,5) <<  D435._depthIntrinsics.coeffs[0],D435._depthIntrinsics.coeffs[1],D435._depthIntrinsics.coeffs[2],D435._depthIntrinsics.coeffs[3],D435._depthIntrinsics.coeffs[4] );

     double distCoeff[5] = { D435._depthIntrinsics.coeffs[0],D435._depthIntrinsics.coeffs[1],D435._depthIntrinsics.coeffs[2],D435._depthIntrinsics.coeffs[3],D435._depthIntrinsics.coeffs[4] };
     // float distCoeff[2]  = {10.0,4.0};
    // cv::Mat distCoeff(1,5,CV_64F, Coeff);










 }







//void ArucoAnalyzer::MarkerFinder(rs2::frame C_frame) =>in funzione sara cosi
void ArucoAnalyzer::MarkerFinder(cv::Mat COLORFRAME)

{
    /*
    //generazione marker
    cv::Mat markerImage;
    cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    cv::aruco::drawMarker(dictionary, 23, 200, markerImage, 1); //23 id marker
    cv::imwrite("marker23.png", markerImage);
    std::cout << "marker23 generated"<<std::endl;
    */

    //cv::Mat inputImage;

    //Marker detection

    std::vector<int> markerIds;
    std::vector<std::vector<cv::Point2f>> markerCorners, rejectedCandidates;
    cv::Ptr<cv::aruco::DetectorParameters> parameters = cv::aruco::DetectorParameters::create();
    cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    cv::aruco::detectMarkers(COLORFRAME, dictionary, markerCorners, markerIds, parameters, rejectedCandidates);

    //draw marker id

    cv::Mat outputImage = COLORFRAME.clone();
    cv::aruco::drawDetectedMarkers(outputImage, markerCorners, markerIds);
    std::vector<cv::Point2f> vector = markerCorners[0];
    pt1 = vector[0];

    std::cout<< "MARKER CORNERS0 ="<<pt1<< "endo" <<std::endl; //il alto a sinistra 0,0 in basso a destra max,max

    cv::imshow("Display window Camera with marker", outputImage);


    //pose extimation
    //camera matrix : 3x3 focal dist and camera center coord
    //dist coeff : 5 or + disortion produced by camera



    std::vector<cv::Vec3d> rvecs, tvecs;
    cv::aruco::estimatePoseSingleMarkers(markerCorners, 0.053, cameraMatrix, distCoeffs, rvecs, tvecs);




    //image with axes draw
    cv::Mat outputImage2;// = COLORFRAME.clone();

    COLORFRAME.copyTo(outputImage2);
    for (int i = 0; i < rvecs.size(); ++i) {
        auto rvec = rvecs[i];
        auto tvec = tvecs[i];
       //cv::aruco::drawAxis(outputImage2, cameraMatrix, distCoeffs, rvec, tvec, 0.1);
    }








    }
void ArucoAnalyzer::ExtractDistance(cv::Mat DepthMat, float DepthScale,cv::Mat ColorMat){
    MarkerFinder(ColorMat);





/*
    float distance1 = (float)DepthMat.at<uint16_t>(pt1.x, pt1.y) * DepthScale;
    std::cout<< "distance ="<<distance1<< "endo" <<std::endl;
*/




















}













 /*
    cv::Ptr<cv::aruco::Dictionary> dictionary = cv::aruco::getPredefinedDictionary(cv::aruco::DICT_6X6_250);
    while (inputVideo.grab()) {
        cv::Mat image, imageCopy;
        inputVideo.retrieve(image);
        image.copyTo(imageCopy);
        std::vector<int> ids;
        std::vector<std::vector<cv::Point2f> > corners;
        cv::aruco::detectMarkers(image, dictionary, corners, ids);
        // if at least one marker detected
        if (ids.size() > 0)
            cv::aruco::drawDetectedMarkers(imageCopy, corners, ids);
        cv::imshow("out", imageCopy);
        char key = (char) cv::waitKey(0);
        if (key == 27)
            break;
    }
    */

