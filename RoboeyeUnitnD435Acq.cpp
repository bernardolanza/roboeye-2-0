#include "RoboeyeUnitnD435Acq.h"


RoboeyeUnitnD435Acq::RoboeyeUnitnD435Acq()
{
    std::cout << "crazione oggetto principale..."<< std::endl;
	// READ SETTING FROM INI FILE
	QSettings setting("config.ini", QSettings::IniFormat);
	setting.beginGroup("/RealSenseD435");
	_acquisitionTimeout = setting.value("AcquisitionTiming").toInt();
	_zmqAckTiming = setting.value("ZmqAckTiming").toInt();
	_zmqAckMasterTimeout = setting.value("ZmqAckMasterTimeout").toInt();
	setting.endGroup();

	// SerialCom Ack Initialization
	_zmqControlAck.SetData(true, 0);
    std::cout << "SERIAL ACK INITIALIZATION" << std::endl;

	// ACK ZMQ CLASSES
    _zmqMasterAckSub = new MIRoZmqSubscriber("tcp://127.0.0.1:10009", _zmqAckMasterTimeout, std::bind(&RoboeyeUnitnD435Acq::ReadZmqMasterAckCallback, this, std::placeholders::_1, std::placeholders::_2), std::bind(&RoboeyeUnitnD435Acq::ReadZmqMasterAckErrorCallback, this, std::placeholders::_1, std::placeholders::_2));
    _zmqAckPub = new MIRoZmqPublisher("tcp://*:10004");

	_zmqAckTimer = new QTimer(this);
	connect(_zmqAckTimer, SIGNAL(timeout()), this, SLOT(ZmqAckTimerTimeOut()));
	_zmqAckTimer->start(_zmqAckTiming);

	// MASTER STATUS
	_zmqMasterStatus.SetData(MiroZmq::NavigationStatus::Idle, 0);							// masterStatus initialization
	_zmqMasterStatusSub = new MIRoZmqSubscriber("tcp://127.0.0.1:5901", std::bind(&RoboeyeUnitnD435Acq::ReadZmqMasterStatus, this, std::placeholders::_1, std::placeholders::_2));

	// KINECT
    _zmqRgbFramePub = new MIRoZmqPublisher("tcp://*:54001");
    _zmqCloudPub = new MIRoZmqPublisher("tcp://*:54002");
    _zmqMapperPub = new MIRoZmqPublisher("tcp://*:54003");

#if VISUALIZE_IMAGE
	//cv::namedWindow("Display window Kinect", cv::WINDOW_AUTOSIZE);// Create a window for display.
#endif

	while (InitializeAcquisition())	{}

    usleep(1000);

	_acquisitionCounter = 0;

//	_acquisitionTimer = new QTimer(this);
//	connect(_acquisitionTimer, SIGNAL(timeout()), this, SLOT(AcquireFrames()));
//	_acquisitionTimer->start(_acquisitionTimeout);

	while (1) {
		AcquireFrames();
        usleep(_acquisitionTimeout);
	}

}


RoboeyeUnitnD435Acq::~RoboeyeUnitnD435Acq()
{
}

void RoboeyeUnitnD435Acq::ReadZmqMasterAckCallback(char* buf, int size)
{
	if (buf[0] == 11)
	{
		_zmqMasterAck = ZmqUtility::DeserializeAck(buf, size);
	}
	else
	{

	}

#if WRITE_CONSOLE
	std::cout << "Setup Ack Master: " << static_cast<unsigned>(_zmqMasterAck._type) << " " << _zmqMasterAck._active << " " << _zmqMasterAck._counter << std::endl;
#endif

}

void RoboeyeUnitnD435Acq::ReadZmqMasterAckErrorCallback(char* buf, int size)
{
	_zmqMasterAck._active = false;

#if WRITE_CONSOLE
	std::cout << "ACK ERROR CALBACK MASTER" << std::endl;
#endif

}

void RoboeyeUnitnD435Acq::ZmqAckTimerTimeOut()
{
	int counter = _zmqControlAck._counter;

	if (counter == 99)
	{
		counter = 0;
	}
	else
	{
		counter += 1;
	}

	_zmqControlAck.SetData(true, counter);

	if (_zmqAckPub != NULL)
	{
		_zmqAckPub->publishAck(_zmqControlAck);
	}

	/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */
	/*cv::Mat image;
	image = cv::imread("image.jpg", CV_LOAD_IMAGE_COLOR);

	if (!image.data)                              // Check for invalid input
	{
	cout << "Could not open or find the image" << std::endl;
	}

	cv::imshow("Display window Kinect", image);                   // Show our image inside it.

	cv::cvtColor(image, image, cv::COLOR_BGR2RGB);
	//cout << "Type: " << image.type() << endl;


	MiroZmq::ZmqMat _zmqRgBImage;
	_zmqRgBImage.SetData(image);

	//_zmqRgbFramePub->publishImage(_zmqRgBImage);

	/* %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  */
}

void RoboeyeUnitnD435Acq::ReadZmqMasterStatus(char* buf, int size)
{
	if (buf[0] == 14)
	{
		QMutexLocker locker(&_masterStatusMutex);
		_zmqMasterStatus = ZmqUtility::DeserializeStatus(buf, size);

		if (_zmqMasterStatus._status == MiroZmq::NavigationStatus::Error)
		{

		}
	}
	else
	{

	}

#if WRITE_CONSOLE
//	std::cout << "Setup Status Master: " << static_cast<unsigned>(_zmqMasterStatus._type) << " " << _zmqMasterStatus._status << " " << _zmqMasterStatus._param << std::endl;
#endif

}

bool RoboeyeUnitnD435Acq::InitializeAcquisition()
{
	std::string SN = "";
	if (!_camera.initializeSensor(cv::Size2i(_depthWidth, _depthHeight), _depthFPS, cv::Size2i(_rbgWidth, _rgbHeight), _rgbFPS, 0.5, false, SN))
	{
#if WRITE_CONSOLE
		std::cerr << "ERROR: " << _camera.getLastError() << std::endl;
		getchar();
#endif
		return true;
	}



	/*if (!_camera.initializeSensor(true, true, false))
	{
#if WRITE_CONSOLE
		std::cout << _camera.getLastError() << std::endl;
		getchar();
#endif
		return true;
	}*/

	return false;
}

int RoboeyeUnitnD435Acq::AcquireFrames()
{
	if (!_camera.waitNewFrame())
	{
		std::cout << _camera.getLastError() << std::endl;
		return -1;
	}

	/*if (!_camera.getColor(&colorMat))
	{
		std::cout << _camera.getLastError() << std::endl;
		return -1;
	}*/

	//if ((_zmqMasterStatus._status == MiroZmq::NavigationStatus::Manual) || (_zmqMasterStatus._status == MiroZmq::NavigationStatus::SemiAuto))
    if ((_zmqMasterStatus._status == MiroZmq::NavigationStatus::Manual))
	{
		if (_acquisitionCounter % 1 == 0)
		{
			if (!_camera.getColor(&colorMat))
			{
				std::cout << _camera.getLastError() << std::endl;
				return -1;
			}

#if VISUALIZE_IMAGE
			cv::Mat img2Show;
			//	cv::cvtColor(colorMat, img2Show, cv::COLOR_BGR2RGB);
			cv::resize(colorMat, colorMat, cv::Size(), 0.5, 0.5);
			cv::imshow("Display window Camera", colorMat);
            std::cout<<"BP 1.0"<<std::endl;
			cv::waitKey(1);
#endif
			cv::cvtColor(colorMat, colorMat, cv::COLOR_BGR2RGB);
			_zmqRgBImage.SetData(colorMat);
			_zmqRgbFramePub->publishRgbFrame(_zmqRgBImage);
		}

	}
	else if ((_zmqMasterStatus._status == MiroZmq::NavigationStatus::SemiAuto) || true)
	{


        if (_acquisitionCounter % 10 == 0)
		{
			if (!_camera.getColor(&colorMat))
			{
				std::cout << _camera.getLastError() << std::endl;
				return -1;
			}

			cv::cvtColor(colorMat, colorMat, cv::COLOR_BGR2RGB);
			_zmqRgBImage.SetData(colorMat);
			_zmqRgbFramePub->publishRgbFrame(_zmqRgBImage);
//		}

#if VISUALIZE_IMAGE
			cv::Mat img2Show;
            //cv::cvtColor(colorMat, img2Show, cv::COLOR_BGR2RGB);
			cv::resize(colorMat, colorMat, cv::Size(), 0.5, 0.5);
            cv::imshow("Display window Camera", colorMat); //________________________________in visualizzazione
            std::cout<<"BP 2.0"<<std::endl;

            //std::cout<<"type:"<<depthMat.type()<<std::endl;
            cv::waitKey(1);
            _camera.getDepth(&depthMat);
            _camera.extract3dPoints();






            ARUCO.ExtractDistance(depthMat,_camera._depth_scale, colorMat);








            //cv::Mat img2Show;
            //cv::cvtColor(colorMat, img2Show, cv::COLOR_BGR2RGB);
           // cv::resize(depthMat, depthMat, cv::Size(), 0.5, 0.5);
           // cv::imshow("Display depth Camera", depthMat); //________________________________in visualizzazione
           // std::cout<<"BP 3.0"<<std::endl;

            //ARUCO.MarkerFinder(colorMat);

#endif
		}

		if (_acquisitionCounter % 10 == 0)
		{
			unsigned short *colorMapX, *colorMapY;
			if (!_camera.getMapper(&colorMapX, &colorMapY))
			{
				std::cout << _camera.getLastError() << std::endl;
				return -1;
			}
			//		if (_acquisitionCounter % 20 == 0)
			//		{
			_zmqMapper.SetData(_rgbHeight, _rbgWidth, (signed short *)colorMapX, (signed short *)colorMapY);
			//		}

			signed short *cloudPointsX, *cloudPointsY, *cloudPointsZ;
			if (!_camera.getPointCloud(&cloudPointsX, &cloudPointsY, &cloudPointsZ))
			{
				std::cout << _camera.getLastError() << std::endl;
				return -1;
			}

			//		if (_acquisitionCounter % 20 == 0)
			//		{
			_zmqCloud.SetData(480, 848, cloudPointsX, cloudPointsY, cloudPointsZ);
			//_zmqRgbFramePub->publishRgbFrame(_zmqRgBImage);
			_zmqMapperPub->publish2DPoints(_zmqMapper);
			_zmqCloudPub->publish3DPoints(_zmqCloud);

		}

		_acquisitionCounter++;
	}
	else if ((_zmqMasterStatus._status == MiroZmq::NavigationStatus::Idle) || (_zmqMasterStatus._status == MiroZmq::NavigationStatus::Error))
	{


		if (_acquisitionCounter % 4 == 0)
		{
			if (!_camera.getColor(&colorMat))
			{
				std::cout << _camera.getLastError() << std::endl;
				return -1;
			}

			cv::cvtColor(colorMat, colorMat, cv::COLOR_BGR2RGB);
			_zmqRgBImage.SetData(colorMat);
			_zmqRgbFramePub->publishRgbFrame(_zmqRgBImage);
			//		}

#if VISUALIZE_IMAGE
			cv::Mat img2Show;
			//	cv::cvtColor(colorMat, img2Show, cv::COLOR_BGR2RGB);
			cv::resize(colorMat, colorMat, cv::Size(), 0.5, 0.5);
			cv::imshow("Display window Camera", colorMat);
            std::cout<<"BP 3.0"<<std::endl;
			cv::waitKey(1);
#endif
		}

		if (_acquisitionCounter % 10 == 0)
		{
			unsigned short *colorMapX, *colorMapY;
			if (!_camera.getMapper(&colorMapX, &colorMapY))
			{
				std::cout << _camera.getLastError() << std::endl;
				return -1;
			}
			//		if (_acquisitionCounter % 20 == 0)
			//		{
			_zmqMapper.SetData(480, 848, (signed short *)colorMapX, (signed short *)colorMapY);
			//		}

			signed short *cloudPointsX, *cloudPointsY, *cloudPointsZ;
			if (!_camera.getPointCloud(&cloudPointsX, &cloudPointsY, &cloudPointsZ))
			{
				std::cout << _camera.getLastError() << std::endl;
				return -1;
			}

			//		if (_acquisitionCounter % 20 == 0)
			//		{
			_zmqCloud.SetData(480, 848, cloudPointsX, cloudPointsY, cloudPointsZ);
			//_zmqRgbFramePub->publishRgbFrame(_zmqRgBImage);
			_zmqMapperPub->publish2DPoints(_zmqMapper);
			_zmqCloudPub->publish3DPoints(_zmqCloud);
		}

		_acquisitionCounter++;
	}



}
