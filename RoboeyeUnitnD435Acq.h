#ifndef ROBOEYEUNITND435ACQ_H
#define ROBOEYEUNITND435ACQ_H

#include "MIRoZmqPublisher.h"
#include "MIRoZmqSubscriber.h"

#include "ZmqUtility.h"

#include "PTRealsense2.h"
#include "arucoanalyzer.h"

#include <qsettings.h>
#include <qobject.h>
#include <qtimer.h>
#include <qthread.h>

#include <qmutex.h>

#define WRITE_CONSOLE 1
#define VISUALIZE_IMAGE 1

using namespace std;

/* SERVICES NUMBERING

	0   --> HMI
	1   --> PLANNING
	2   --> CONTROL, PATH FOLLOWING
	3   --> SERIAL COMUNICATION
	4.1 --> MICROSOFT KINECT
	4.2 --> MICROSOFT REAL SENSE
	5   --> AUTONOMOUS NAVIGATION
	9   --> MASTER SERVICE

*/

class RoboeyeUnitnD435Acq : public QObject
{
	Q_OBJECT
public:
	RoboeyeUnitnD435Acq();
	~RoboeyeUnitnD435Acq();
    ArucoAnalyzer ARUCO;
    struct PC {
        signed short *x;
        signed short *y;
        signed short *z;
        unsigned char *r;
        unsigned char *g;
        unsigned char *b;
        size_t size;
    };

private:
	//ACK CLASSES
	MIRoZmqSubscriber *_zmqMasterAckSub;
    MIRoZmqPublisher *_zmqAckPub; // Ack sent automatically each 200 ms (Qtimer event generation)


	MiroZmq::ZmqAck _zmqMasterAck;
	MiroZmq::ZmqAck _zmqControlAck;

	QTimer *_zmqAckTimer;

	int _zmqAckTiming;
	int _zmqAckMasterTimeout;

	void ReadZmqMasterAckCallback(char* buf, int size);
	void ReadZmqMasterAckErrorCallback(char* buf, int size);


	// MASTER STATUS
	MIRoZmqSubscriber *_zmqMasterStatusSub;

	MiroZmq::ZmqStatus _zmqMasterStatus;
	QMutex _masterStatusMutex;

	void ReadZmqMasterStatus(char* buf, int size);

	// REALSENSE D435
	MIRoZmqPublisher *_zmqRgbFramePub;
	MIRoZmqPublisher *_zmqCloudPub;
	MIRoZmqPublisher *_zmqMapperPub;

	PTRealsense2 _camera;

	int _rbgWidth = 1280;
	int _rgbHeight = 720;
	int _depthWidth = 848;
	int _depthHeight = 480;

	int _rgbFPS		= 30;
	int _depthFPS	= 30;

	cv::Mat depthMat, colorMat, depthOverColorMap;
	QTimer *_acquisitionTimer;

	MiroZmq::ZmqMat _zmqRgBImage;
	MiroZmq::Zmq3DPoints _zmqCloud;
	MiroZmq::Zmq2DPoints _zmqMapper;

	int _acquisitionTimeout;

	bool InitializeAcquisition();

	int _acquisitionCounter;

public slots:
	void ZmqAckTimerTimeOut();
	int AcquireFrames();

};

#endif
