#include "vot265realsense.h"
#include <iostream>
#include <unistd.h>







VOT265Realsense::VOT265Realsense()
{
std::cout << "VOT vostruito"<< std::endl;
}
/*
VOT265Realsense::~VOT265Realsense()
{

}
*/


  void VOT265Realsense::calcprintXYZ()
{
      std::cout <<"calcprint XYZ ..." <<std::endl;

    cfg.enable_stream(RS2_STREAM_POSE, RS2_FORMAT_6DOF);// Add pose stream
    cfg.enable_stream(RS2_STREAM_GYRO);
    cfg.enable_stream(RS2_STREAM_ACCEL);
    pipe.start(cfg);// Start pipeline with chosen configuration
    int i = 0;

    cv::Mat imageToDraw(map_dim,map_dim, CV_8UC3, cv::Scalar(0, 0, 0));
    cv::Mat img2Show;
    //cv::cvtColor(colorMat, img2Show, cv::COLOR_BGR2RGB);
    //cv::resize(imageToDraw, imageToDraw, cv::Size(), 2, 2);




    while (true)

    {


        // Wait for the next set of frames from the camera
        frames = pipe.wait_for_frames();
        // Get a frame from the pose stream
        //
        //rs2::depth_frame depthFrame = _frames.get_depth_frame();// esempio per evitare errore dichiarazione tipologia di frame
        rs2::pose_frame frameAlphaPose = frames.first_or_default(RS2_STREAM_POSE);

        if (rs2::motion_frame accel_frame = frames.first_or_default(RS2_STREAM_ACCEL))
        {
            accel_sample = accel_frame.get_motion_data();
           // std::cout << "Accel:" << accel_sample.x << ", " << accel_sample.y << ", " << accel_sample.z << std::endl;

        }

        if (rs2::motion_frame gyro_frame = frames.first_or_default(RS2_STREAM_GYRO))
        {
            gyro_sample = gyro_frame.get_motion_data();
            //std::cout << "Gyro:" << gyro_sample.x << ", " << gyro_sample.y << ", " << gyro_sample.z << std::endl;

        }
        pose = frameAlphaPose.as<rs2::pose_frame>().get_pose_data();


       // std::cout << "translation x , y , z : " << pose.rotation.y <<  std::endl ;
        usleep(1000);






        pt.x = (pose.translation.x*zoom)+map_dim/2;
        pt.y = (pose.translation.z*zoom)+map_dim/2;

        trj[i]  = pt;//bella
        angle = pose.rotation.y * PI-PI/2;
        std::cout <<"angle : "<< angle << std::endl;
        pax.x = pt.x-l*cos(angle);
        pax.y = pt.y+l*sin(angle);
        pay.x = pt.x-l*cos(angle+PI/2);
        pay.y = pt.y+l*sin(angle+PI/2);





          if(i>1){
          cv::line(imageToDraw,trj[i-1],trj[i],cv::Scalar( 0, 0, 255 ),thickness);
          imageToDraw.copyTo(aximage);

          cv::line(aximage,trj[i],pax,cv::Scalar( 0, 255, 0),thickness);
          cv::line(aximage,trj[i],pay,cv::Scalar( 255, 0, 0),thickness);

          cv::imshow("Display window Camera", aximage); //in visualizzazione

          cv::waitKey(1);

               }


          i++;






    /*    std::vector<cv::Point> pointsInLast20Frames; //fill this vector with points, they should be ordered
        pointsInLast20Frames[i] = {pose.translation.x,pose.translation.y};


        cv::Scalar color(0, 0, 255); //red

           cv::line(imageToDraw, pointsInLast20Frames[i], pointsInLast20Frames[i+1], color);
          // cv::imshow("Display window Camera", imageToDraw); //in visualizzazione
           i++;*/



    }


}

void VOT265Realsense::AllSenseMode()
{
    std::cout <<"all sense mode...." <<std::endl;

//disabilitato pe testere i permessi di paternità con cameras interaction



    cfg.enable_stream(RS2_STREAM_GYRO);
    cfg.enable_stream(RS2_STREAM_ACCEL);
    cfg.enable_stream(RS2_STREAM_POSE);
    std::cout<<"BP cfg enable stream"<<std::endl;
    pipeline = pipe.start(cfg);
    std::cout<<"BP pipe start"<<std::endl;
    while (true) // Application still alive?
    {
        frames = pipe.wait_for_frames();
        std::cout<<"pipe wait for frames"<<std::endl;

        // Find and retrieve IMU and/or tracking data


        if (rs2::pose_frame frameAlphaPose = frames.first_or_default(RS2_STREAM_POSE))
        {
            pose = frameAlphaPose.get_pose_data();
            std::cout << "Pose:" << pose.translation.x << ", " << pose.translation.y << ", " << pose.translation.z << std::endl;

        }



    }
}

void VOT265Realsense::DrawTrajectories()
{

    cv::Mat imageToDraw; //this is your image to draw, don't forget to load it
    std::vector<cv::Point> pointsInLast20Frames; //fill this vector with points, they should be ordered

    cv::Scalar color(0, 0, 255); //red
    for(int i = 0; i < pointsInLast20Frames.size() - 1; ++i)
    {
       cv::line(imageToDraw, pointsInLast20Frames[i], pointsInLast20Frames[i+1], color);
    }


}

