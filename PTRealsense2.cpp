#include "PTRealsense2.h"

#define CLAMP(t,min,max) (((t)>max)?max:(((t)<min)?min:(t)))

PTRealsense2::PTRealsense2()
{
	cv::setUseOptimized(true);

	_uid = "undefined";
	_lastErrorString = "";
	_newFramePresent = false;

	_wantStreamDepth = false;
	_wantStreamColor = false;
	_wantStreamIr = false;

	_depthReaded = false;
	_colorReaded = false;
	_irReaded = false;
	
    
	

	

	/*
	// Declare disparity transform from depth to disparity and vice versa
	rs2::disparity_transform depth_to_disparity;
	rs2::disparity_transform disparity_to_depth(false);

	// initialize a vector that would allow iterating on possible filter options
	std::vector<rs2_option> option_names = { RS2_OPTION_FILTER_MAGNITUDE, RS2_OPTION_FILTER_SMOOTH_ALPHA,
		RS2_OPTION_FILTER_SMOOTH_DELTA };
	


	// initialize a vector that holds filters and their option values
	filter_options dec_struct;
	dec_struct.filter = &dec_filter;
	filter_options disparity_struct;
	disparity_struct.filter = &depth_to_disparity;
	filter_options spat_struct;
	spat_struct.filter = &spat_filter;
	filter_options temp_struct;
	temp_struct.filter = &temp_filter;
	filters = { dec_struct, disparity_struct, spat_struct, temp_struct };

	// set parameters that are controlled by the user to default values
	set_defaults(filters, option_names);

	//filters[3].float_params[RS2_OPTION_FILTER_SMOOTH_ALPHA] = 0.3;

	*/
}

// Set default values for the parameters that control filter options through the interface
/*void PTRealsense2::set_defaults(std::vector<filter_options>& filters, const std::vector<rs2_option>& option_names) {
	for (int i = 0; i < filters.size(); i++) {
		for (int j = 0; j < option_names.size(); j++) {
			if (filters[i].filter->supports(option_names[j])) {
				rs2::option_range range = filters[i].filter->get_option_range(option_names[j]);
				if (is_all_integers(range.min, range.max, range.def, range.step))
				{
					filters[i].int_params[option_names[j]] = range.def;
					int value = filters[i].int_params[option_names[j]];
				}
				else
				{
					float value = filters[i].int_params[option_names[j]];
					filters[i].float_params[option_names[j]] = range.def;
				}
			}
		}
		filters[i].do_filter = true;
	}
}*/

PTRealsense2::~PTRealsense2()
{
}

std::string PTRealsense2::getLastError() {
	return _lastErrorString;
}

std::string PTRealsense2::getUid() {

	return _uid;
}

void PTRealsense2::printD435Resolutions() {

	std::cout << "Possible resolution [COLS, ROWS (FPS)]" << std::endl;
	std::cout << "Depth:" << std::endl;
	std::cout << "  424,240 (6,15,20,30,60,90)" << std::endl;
	std::cout << "  480,270 (6,15,20,30,60,90)" << std::endl;
	std::cout << "  480,270 (6,15,20,30,60,90)" << std::endl;
	std::cout << "  640,360 (6,15,20,30,60,90)" << std::endl;
	std::cout << "  848,480 (6,15,20,30,60,90)" << std::endl;
	std::cout << "  1280,720 (6, 15,20,30)" << std::endl;

	std::cout << "Color:" << std::endl;
	std::cout << "  320,180 (6,15,30,60)" << std::endl;
	std::cout << "  424,240 (6,15,30,60)" << std::endl;
	std::cout << "  480,270 (6,15,30,60)" << std::endl;
	std::cout << "  480,270 (6,15,30,60)" << std::endl;
	std::cout << "  640,360 (6,15,30,60)" << std::endl;
	std::cout << "  848,480 (6,15,30,60)" << std::endl;
	std::cout << "  1280,720 (6,15,30)" << std::endl;
	std::cout << "  1920,1080 (6,15,30)" << std::endl;


}

// initialize sensor
bool PTRealsense2::initializeSensor(cv::Size2i depthRowCol, int depthFPS, cv::Size2i colorRowCol, int colorFPS, float tempFilterAlpha, bool wantIR, std::string sn) {

	_wantStreamDepth = depthRowCol.height > 0 && depthRowCol.width > 0;
	_wantStreamColor = colorRowCol.height > 0 && colorRowCol.width > 0;
	_wantStreamIr = wantIR;

	if (PTRS2_VERBOSE) printD435Resolutions();

	rs2::device_list devices = _context.query_devices(); // Get a snapshot of currently connected devices
	if (devices.size() == 0)	{
		_lastErrorString = "No device detected. Is it plugged in?";
		return false;
	}		

	if (_wantStreamDepth) {
		_cfg.enable_stream(RS2_STREAM_DEPTH, depthRowCol.width, depthRowCol.height, RS2_FORMAT_Z16, depthFPS);
		if (PTRS2_VERBOSE) std::cout << "Depth SIZE (C,R,FPS)= " << depthRowCol.width << "," << depthRowCol.height << "," << depthFPS << std::endl;

	}
	if (_wantStreamColor) {
		_cfg.enable_stream(RS2_STREAM_COLOR, colorRowCol.width, colorRowCol.height, RS2_FORMAT_BGR8, colorFPS);
		if (PTRS2_VERBOSE) std::cout << "Color SIZE (C,R,FPS)= " << colorRowCol.width << "," << colorRowCol.height << "," << colorFPS << std::endl;

	}
	if (_wantStreamIr) {
		_cfg.enable_stream(RS2_STREAM_INFRARED, depthRowCol.width, depthRowCol.height, RS2_FORMAT_Y8, depthFPS);
		if (PTRS2_VERBOSE) std::cout << "Infrared SIZE (C,R,FPS)= " << depthRowCol.width << "," << depthRowCol.height << "," << depthFPS << std::endl;

	}

	
	if (sn != "") {

		_uid = sn;
		bool found = false;

		if (PTRS2_VERBOSE) std::cout << "found " << devices.size() << " realsense devices." << std::endl;

        for (size_t i = 0; i < devices.size(); i++) {
			rs2::device dev = devices[i];
			std::string devSerialNumber = dev.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
			
			if (PTRS2_VERBOSE) std::cout << "dev " << i << " -> SN=" << devSerialNumber << std::endl;

			if (devSerialNumber == _uid) {
				found = true;
				break;
			}
		}
		if (!found) {
			_lastErrorString = "No Realsense camera with SN=" + _uid + " found!";
			return false;
		}
        _cfg.enable_device(_uid);

	}

	// Start streaming with default recommended configuration
	try {
        _profile = _pipe.start(_cfg);
	}
	catch (const rs2::error & e) {
		_lastErrorString = "Error Starting Pipeline! " + e.get_failed_function() + ". Maybe wrong resolution!";
		return false;
	}
	_device = _profile.get_device();
	
	if (_device.supports(RS2_CAMERA_INFO_SERIAL_NUMBER)) {
		_uid = _device.get_info(RS2_CAMERA_INFO_SERIAL_NUMBER);
		if (PTRS2_VERBOSE) std::cout << "Sterted device with SN= " << _uid << std::endl;

	}
		

	rs2::sensor depthSensor = _device.first<rs2::depth_sensor>();
	_depth_scale = depthSensor.as<rs2::depth_sensor>().get_depth_scale();
	if (PTRS2_VERBOSE) std::cout << "Depth Scale (mm)= " << _depth_scale << std::endl;
	
	if (_wantStreamIr) {

		// ir intrinsics
		rs2::video_stream_profile irStream = _profile.get_stream(RS2_STREAM_INFRARED).as<rs2::video_stream_profile>();
		_irIntrinsics = irStream.get_intrinsics();

		float fov[2]; // X, Y fov
		rs2_fov(&_irIntrinsics, fov);
		if (PTRS2_VERBOSE) std::cout << "Infrared FOV (h,v)= " << fov[0] << "," << fov[1] << std::endl;
	}

	if (_wantStreamDepth) {

		// depth intrinsics
		rs2::video_stream_profile depthStream = _profile.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>();
		_depthIntrinsics = depthStream.get_intrinsics();		

		float fov[2]; // X, Y fov
		rs2_fov(&_depthIntrinsics, fov);
		if (PTRS2_VERBOSE) std::cout << "Depth FOV (h,v)= " << fov[0] << "," << fov[1] << std::endl;


		initializeArray(&(_pointCloud.x), depthRowCol.width * depthRowCol.height, 0);
		initializeArray(&(_pointCloud.y), depthRowCol.width * depthRowCol.height, 0);
		initializeArray(&(_pointCloud.z), depthRowCol.width * depthRowCol.height, 0);
		initializeArray(&(_pointCloud.r), depthRowCol.width * depthRowCol.height, 255);
		initializeArray(&(_pointCloud.g), depthRowCol.width * depthRowCol.height, 255);
		initializeArray(&(_pointCloud.b), depthRowCol.width * depthRowCol.height, 255);
		initializeArray(&(_colorPixelR), depthRowCol.width * depthRowCol.height, 0);
		initializeArray(&(_colorPixelC), depthRowCol.width * depthRowCol.height, 0);

		temp_filter.set_option(RS2_OPTION_FILTER_SMOOTH_ALPHA, tempFilterAlpha);
		temp_filter.set_option(RS2_OPTION_FILTER_SMOOTH_DELTA, 20);
//		if (PTRS2_VERBOSE) std::cout << "Depth smooth filter active with alpha = " << tempFilterAlpha << std::endl;

//		spat_filter.set_option(RS2_OPTION_FILTER_MAGNITUDE, 2.0f);
//		spat_filter.set_option(RS2_OPTION_FILTER_SMOOTH_ALPHA, 0.5f);
//		spat_filter.set_option(RS2_OPTION_FILTER_SMOOTH_DELTA, 20);

//		dec_filter.set_option(RS2_OPTION_FILTER_MAGNITUDE, 2.0f);
	}

	if (_wantStreamColor) {

		// color intrinsics
		rs2::video_stream_profile colorStream = _profile.get_stream(RS2_STREAM_COLOR).as<rs2::video_stream_profile>();
		_colorIntrinsics = colorStream.get_intrinsics();

		float fov[2]; // X, Y fov
		rs2_fov(&_colorIntrinsics, fov);
		if (PTRS2_VERBOSE) std::cout << "Color FOV (h,v)= " << fov[0] << "," << fov[1] << std::endl;

	}

	if (_wantStreamDepth && _wantStreamColor) {		
		// color 2 depth extrinsics
		rs2::video_stream_profile colorStream = _profile.get_stream(RS2_STREAM_COLOR).as<rs2::video_stream_profile>();
		rs2::video_stream_profile depthStream = _profile.get_stream(RS2_STREAM_DEPTH).as<rs2::video_stream_profile>();

		_c2dExtrinsics = colorStream.get_extrinsics_to(depthStream);
		_d2cExtrinsics = depthStream.get_extrinsics_to(colorStream);
		

        _mapperX = cv::Mat(colorRowCol.height, colorRowCol.width, CV_16UC1).setTo(0);
        _mapperY = cv::Mat(colorRowCol.height, colorRowCol.width, CV_16UC1).setTo(0);
	}	

	return true;
}

// release sensor variables
void PTRealsense2::releaseSensor() {

    _pipe.stop();
	if (PTRS2_VERBOSE) std::cout << "Stream closed." << std::endl;


	// close windows
	if (PTRS2_VISUALIZE_IMAGE) cv::destroyAllWindows();
}

bool PTRealsense2::waitNewFrame() {
	
    _newFramePresent = false;
    _depthReaded = false;
    _colorReaded = false;
    _irReaded = false;
    _pointCloudCalculated = false;
    _mappingCalculated = false;
    _pointCloud.size = 0;

	try {
		if(PTRS2_VERBOSE) std::cout << "Waiting for a new Frame...";
        _frames = _pipe.wait_for_frames(500);
		_newFramePresent = true;
		if (PTRS2_VERBOSE) std::cout << "Arrived!" << std::endl;
		return true;
	}
	catch (const rs2::error & e) {
		_lastErrorString = "TIMEOUT! no new frame present! " + e.get_failed_function();
        return false;
	}

}

bool PTRealsense2::getDepth(cv::Mat *image)
{
    if (!_depthReaded) {
        if (!grabDepth()) {
            return false;
        }
    }

    if (image == nullptr) {
        return false;
    }

    *image = _depthMat;

    return true;
}

bool PTRealsense2::getColor(cv::Mat *image)
{
    if (!_colorReaded) {
        if (!grabColor()) {
            return false;
        }
    }

    if (image == nullptr) {
        return false;
    }

    *image = _colorMat;

    return true;
}

bool PTRealsense2::getIr(cv::Mat *image)
{
    if (!_irReaded) {
        if (!grabIr()) {
            return false;
        }
    }

    if (image == nullptr) {
        return false;
    }

    *image = _irMat;

    return true;
}

bool PTRealsense2::grabDepth()
{
    if (!_newFramePresent) {
        _lastErrorString = "No new frame present!";
        return false;
    }

    // read the data from sensor
    try {
		rs2::depth_frame depthFrame = _frames.get_depth_frame();
					
		//depthFrame = dec_filter.process(depthFrame);
		//depthFrame = disp_filter.process(depthFrame);
//		depthFrame = spat_filter.process(depthFrame);
//		depthFrame = temp_filter.process(depthFrame);
		
		// check mat dimension
		if (_depthMat.rows != depthFrame.get_height() || _depthMat.cols != depthFrame.get_width() || _depthMat.type() != CV_16UC1) {
			_depthMat = cv::Mat(depthFrame.get_height(), depthFrame.get_width(), CV_16UC1);
		}

		// copy data pointer to internal var
		_depthMat.data = (uchar*)depthFrame.get_data();
		//cv::imshow("DEPTH", _depthMat);

		/*bool do_decimate = filters[0].do_filter;
		bool do_disparity = filters[1].do_filter;
		bool do_spatial = filters[2].do_filter;
		bool do_temporal = filters[3].do_filter;*/


		/* Apply filters. The implemented flow of the filters pipeline is in the following order: apply decimation filter,
		transform the scence into disparity domain, apply spatial filter, apply temporal filter, revert the results back
		to depth domain (each post processing block is optional and can be applied independantly).
		*/
//		if (do_decimate)
//			filtered = static_cast<rs2::decimation_filter*>(filters[0].filter)->process(filtered);

//		if (do_disparity)
//			filtered = static_cast<rs2::disparity_transform*>(filters[1].filter)->process(filtered);

//		if (do_spatial)
//			filtered = static_cast<rs2::spatial_filter*>(filters[2].filter)->process(filtered);

		//if (do_temporal)
		//	filtered = static_cast<rs2::temporal_filter*>(filters[3].filter)->process(filtered);

		//if (do_disparity)
		//	filtered = disparity_to_depth.process(filtered);


    }
    catch (const rs2::error & e) {
        _lastErrorString = "Sensor initialized without depth! " + e.get_failed_function();
        return false;
    }

    

    // visualize it
    if (PTRS2_VISUALIZE_IMAGE) cv::imshow("SENSOR_DEPTH", _depthMat);

    _depthReaded = true;
    return true;
}

bool PTRealsense2::grabColor()
{
    if (!_newFramePresent) {
        _lastErrorString = "No new frame present!";
        return false;
    }

    // read the data from sensor
    try {
		rs2::video_frame colorFrame = _frames.get_color_frame();

		// check mat dimension
		if (_colorMat.rows != colorFrame.get_height() || _colorMat.cols != colorFrame.get_width() || _colorMat.type() != CV_8UC3) {
			_colorMat = cv::Mat(colorFrame.get_height(), colorFrame.get_width(), CV_8UC3);
		}

		// copy data pointer to internal var
		_colorMat.data = (uchar*)colorFrame.get_data();

    }
    catch (const rs2::error & e) {
        _lastErrorString = "Sensor initialized without color! " + e.get_failed_function();
        return false;
    }

    
    // visualize it
    if (PTRS2_VISUALIZE_IMAGE) cv::imshow("SENSOR_COLOR", _colorMat);

    _colorReaded = true;
    return true;
}

bool PTRealsense2::grabIr()
{
    if (!_newFramePresent) {
        _lastErrorString = "No new frame present!";
        return false;
    }

    // read the data from sensor
    try {
		rs2::video_frame irFrame = _frames.get_infrared_frame();

		// check mat dimension
		if (_irMat.rows != irFrame.get_height() || _irMat.cols != irFrame.get_width() || _irMat.type() != CV_8UC1) {
			_irMat = cv::Mat(irFrame.get_height(), irFrame.get_width(), CV_8UC1);
		}

		// copy data pointer to internal var
		_irMat.data = (uchar*)irFrame.get_data();
    }
    catch (const rs2::error & e) {
        _lastErrorString = "Sensor initialized without ir! " + e.get_failed_function();
        return false;
    }

    

    // visualize it
    if (PTRS2_VISUALIZE_IMAGE) cv::imshow("SENSOR_IR", _irMat);

    _irReaded = true;
    return true;
}

bool PTRealsense2::grabPointCloud(bool coloured)
{
    // get the depth frame
    if (!_depthReaded) {
        if (!grabDepth()) {
            return false;
        }
    }

    // extract 3D position
    if (!_pointCloudCalculated) {
        if (!extract3dPoints()) {
            return false;
        }
    }

    if (!coloured) {
        return true; // check if the color is needed
    }

    // get the color frame
    if (!_colorReaded) {
        if (!grabColor()) {
            return false;
        }
    }

    // calc the mapping
    if (!_mappingCalculated) {
        if (!mapColorPoints()) {
            return false;
        }
    }

    // add the color to the cloud
    if (!addColorToCloud()) {
        return false;
    }

    return true;
}

bool PTRealsense2::getTime(signed long long *time)
{
    if (!_newFramePresent) {
        _lastErrorString = "No new frame present!";
        return false;
    }

    // copy to external var
    if (time != nullptr) {
        *time = (signed long long)_frames.get_timestamp();
    }

    return true;
}



bool PTRealsense2::extract3dPoints()
{
    float depthPixel[2];
    float distance;
    float depthPoint[3];

    for (int r = 0; r < _depthMat.rows; r++) {
        for (int c = 0; c < _depthMat.cols; c++) {
            depthPixel[0] = (float)c;
			depthPixel[1] = (float)r;
            distance = (float)_depthMat.at<uint16_t>(r, c) * _depth_scale;
            rs2_deproject_pixel_to_point(depthPoint, &_depthIntrinsics, depthPixel, distance);
            _pointCloud.x[r * _depthMat.cols + c] = -(signed short)(depthPoint[0] * 1000);
            _pointCloud.y[r * _depthMat.cols + c] = -(signed short)(depthPoint[1] * 1000);
            _pointCloud.z[r * _depthMat.cols + c] = (signed short)(depthPoint[2] * 1000);
/*
            if (r<10 && c<10){
          std::cout<<"pixel 20-20 coordinatexyz:   "<<depthPoint[0]*1000<<"   "<<depthPoint[1]<<"   "<<depthPoint[2]<<std::endl;
            }*/
            std::cout<< "intrinsics dist coesff 0,1"<< _depthIntrinsics.coeffs[2]<< "  "<<_depthIntrinsics.coeffs[4]<<std::endl;



        }
    }

    _pointCloud.size = _depthMat.rows * _depthMat.cols;
    _pointCloudCalculated = true;
    return true;
}

bool PTRealsense2::mapColorPoints() {
	
    float depthPoint[3];
    float colorPoint[3];
    float colorPixel[2];
    int idx = 0;
    for (int r = 0; r < _depthMat.rows; r++) {
        for (int c = 0; c < _depthMat.cols; c++) {
            depthPoint[0] = -(float)_pointCloud.x[r * _depthMat.cols + c] / 1000.0f;
            depthPoint[1] = -(float)_pointCloud.y[r * _depthMat.cols + c] / 1000.0f;
            depthPoint[2] = (float)_pointCloud.z[r * _depthMat.cols + c] / 1000.0f;
            rs2_transform_point_to_point(colorPoint, &_d2cExtrinsics, depthPoint);
            rs2_project_point_to_pixel(colorPixel, &_colorIntrinsics, colorPoint);
            _colorPixelR[idx] = (int)colorPixel[1];
            _colorPixelC[idx]  = (int)colorPixel[0];
            idx++;
        }
    }
    _mappingCalculated = true;

    return true;
}

bool PTRealsense2::addColorToCloud()
{
    cv::Vec3b color;
    int idx = 0;
    for (int r = 0; r < _depthMat.rows; r++) {
        for (int c = 0; c < _depthMat.cols; c++) {
            if ((_colorPixelR[idx] >= 0) && (_colorPixelR[idx] < _depthMat.rows) && (_colorPixelC[idx] >= 0) && (_colorPixelC[idx] < _depthMat.cols)) {
                color = _colorMat.at<cv::Vec3b>(_colorPixelR[idx], _colorPixelC[idx]);
                _pointCloud.r[r * _depthMat.cols + c] = color.val[2];
                _pointCloud.g[r * _depthMat.cols + c] = color.val[0];
                _pointCloud.b[r * _depthMat.cols + c] = color.val[1];
            }
            idx++;
        }
    }
    return true;
}

bool PTRealsense2::addColorToMapper()
{
	_mapperX.setTo(0);
	_mapperY.setTo(0);
    int idx = 0;
	for (int r = 0; r < _depthMat.rows; r++) {
		for (int c = 0; c < _depthMat.cols; c++) {
			if ((_colorPixelR[idx] >= 0) && (_colorPixelR[idx] < _colorMat.rows) && (_colorPixelC[idx] >= 0) && (_colorPixelC[idx] < _colorMat.cols)) {
                _mapperX.at<uint16_t>(_colorPixelR[idx], _colorPixelC[idx]) = c;
                _mapperY.at<uint16_t>(_colorPixelR[idx], _colorPixelC[idx]) = r;
            }
            idx++;
        }
    }
    cv::dilate(_mapperX, _mapperX, cv::Mat(3, 3, CV_8UC1), cv::Point(-1, -1), 1, cv::BORDER_REPLICATE);
    cv::dilate(_mapperY, _mapperY, cv::Mat(3, 3, CV_8UC1), cv::Point(-1, -1), 1, cv::BORDER_REPLICATE);

    return true;
}



size_t PTRealsense2::getPointCloud(signed short **x, signed short **y, signed short **z, unsigned char **r, unsigned char **g, unsigned char **b) {

    // check input
    if (x == nullptr || y == nullptr || z == nullptr) {
        return 0;
    }

    // check if the color is needed
    bool coloured = true;
    if (r == nullptr || g == nullptr || b == nullptr) {
        coloured = false;
    }

    // calc the cloud
    if (!grabPointCloud(coloured)) {
        return 0;
    }

    if (x != nullptr) *x = _pointCloud.x;
    if (y != nullptr) *y = _pointCloud.y;
    if (z != nullptr) *z = _pointCloud.z;

    if (r != nullptr) *r = _pointCloud.r;
    if (g != nullptr) *g = _pointCloud.g;
    if (b != nullptr) *b = _pointCloud.b;

    return _pointCloud.size;
}

int PTRealsense2::getMapper(unsigned short **x, unsigned short **y) {

    // get the depth frame
    if (!_depthReaded) {
        grabDepth();
    }

    // extract 3D position
    if (!_pointCloudCalculated) {
        extract3dPoints();
    }

    // get the color frame
    if (!_colorReaded) {
        grabColor();
    }

    // calc the mapping
    if (!_mappingCalculated) {
        mapColorPoints();
    }

    addColorToMapper();

    if (x != nullptr) *x = (unsigned short*)_mapperX.data;
    if (y != nullptr) *y = (unsigned short*)_mapperY.data;

    return _mapperX.rows * _mapperX.cols;
}

void PTRealsense2::initializeArray(signed short **array, size_t size, int value) {

    *array = new signed short[size];
    for (size_t i = 0; i < size; i++) {
        (*array)[i] = (signed short)value;
    }
}

void PTRealsense2::initializeArray(unsigned char **array, size_t size, int value) {

    *array = new unsigned char[size];
    for (size_t i = 0; i < size; i++) {
        (*array)[i] = (unsigned char)value;
    }
}

void PTRealsense2::initializeArray(unsigned short **array, size_t size, int value) {

    *array = new unsigned short[size];
    for (size_t i = 0; i < size; i++) {
        (*array)[i] = (unsigned short)value;
    }
}

void PTRealsense2::initializeArray(int **array, size_t size, int value) {

    *array = new int[size];
    for (size_t i = 0; i < size; i++) {
        (*array)[i] = value;
    }
}













